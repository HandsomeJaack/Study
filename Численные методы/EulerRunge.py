import matplotlib.pyplot as plt
import numpy as np
import math as mt

x0 = 0;
y0 = 1;
y0_p = 0;
h = 0.1;
ry1 = [1,0,0,0,0,0,0,0,0,0,0];
ry2 = [1,0,0,0,0,0,0,0,0,0,0];
x = [0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1]

def exact_solution(x):
	return x*np.sin(x) + np.cos(x);

def F(x,y):
	return 2*mt.cos(x) - y;

def Euler():
	for i in range(10):
		ry1[i+1] = ry1[i] + h*F(x[i], ry1[i])
	return ry1;

def Runge_Kutt():
	for i in range(10):
		k1 = F(x[i], ry2[i]);
		k2 = F(x[i] + h/2, ry2[i] + h*k1/2);
		k3 = F(x[i] + h/2, ry2[i] + h*k2/2);
		k4 = F(x[i] + h, ry2[i] + h*k3);
		dy = h*(k1 + 2*k2 + 2*k3 + k4)/6;
		ry2[i+1] = ry2[i] + dy;
	return ry2;

if __name__ == "__main__":
	y0 = Euler();
	y1 = Runge_Kutt();
	data = [(x[0], x[1]), (y0[0],y0[1]), 'r',
			(x[1], x[2]), (y0[1],y0[2]), 'r',
			(x[2], x[3]), (y0[2],y0[3]), 'r',
			(x[3], x[4]), (y0[3],y0[4]), 'r',
			(x[4], x[5]), (y0[4],y0[5]), 'r',
			(x[5], x[6]), (y0[5],y0[6]), 'r',
			(x[6], x[7]), (y0[6],y0[7]), 'r',
			(x[7], x[8]), (y0[7],y0[8]), 'r',
			(x[8], x[9]), (y0[8],y0[9]), 'r',
			(x[9], x[10]), (y0[9],y0[10]), 'r']
	data1 = [(x[0], x[1]), (y1[0],y1[1]), 'g',
			(x[1], x[2]), (y1[1],y1[2]), 'g',
			(x[2], x[3]), (y1[2],y1[3]), 'g',
			(x[3], x[4]), (y1[3],y1[4]), 'g',
			(x[4], x[5]), (y1[4],y1[5]), 'g',
			(x[5], x[6]), (y1[5],y1[6]), 'g',
			(x[6], x[7]), (y1[6],y1[7]), 'g',
			(x[7], x[8]), (y1[7],y1[8]), 'g',
			(x[8], x[9]), (y1[8],y1[9]), 'g',
			(x[9], x[10]), (y1[9],y1[10]), 'g']

	print("|	X 	|	Точное решение		|		Метод Эйлера	|	Метод Рунге-Кутта	|		Погрешности 				|")
	for i in range(11):
		print("|	{}	| 	{}	|	{} 	|	{}	|	{} , {}	|".format(x[i], exact_solution(x[i]),
			y0[i], y1[i], abs(exact_solution(x[i]) - y0[i]), abs(exact_solution(x[i]) - y1[i])));
	xp = np.arange(0, 1.2, 0.1)
	plt.plot(xp, exact_solution(xp));
	plt.plot(*data)
	plt.plot(*data1)
	plt.axis([0,1,-2,2]);
	plt.show()
