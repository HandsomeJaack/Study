import math

def f(x):
    return math.log(x+2) - x*x

def f1(x):
    return 1/(x+2) - 2*x

def newton(border):
    ITER = 0
    x0 = border
    x1 = x0 - (f(x0) / f1(x0))
    while True:
        x1 = x0 - (f(x0) / f1(x0))
        if abs(x1 - x0) < 0.0001:
            print("Final result is " + str(x1))
            return x1
        else:
            ITER += 1
            print(str(ITER) + ": " + str(x1))
        x0 = x1

newton(-1.5)
newton(3)


