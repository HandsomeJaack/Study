import math
import numpy
import matplotlib.pyplot as plt

def Func1(x1,x2):
    return x1 - math.cos(x2) -1

def Func2(x1,x2):
    return x2 - math.log(x1+1,10) - 1

def DFunc1X1(x1,x2):
    return 1

def DFunc1X2(x1,x2):
    return math.sin(x2)

def DFunc2X1(x1,x2):
    return 1/((x1+1)*math.log(10))

def DFunc2X2(x1,x2):
    return 1

def Newton(x1_0, x2_0):
    J = DFunc1X1(x1_0, x2_0) * DFunc2X2(x1_0, x2_0) - DFunc1X2(x1_0, x2_0) * DFunc2X1(x1_0, x2_0)
    A1 = Func1(x1_0, x2_0) * DFunc2X2(x1_0, x2_0) - DFunc1X2(x1_0, x2_0) * Func2(x1_0, x2_0)
    A2 = DFunc1X1(x1_0, x2_0) * Func2(x1_0, x2_0) - Func1(x1_0, x2_0) * DFunc2X1(x1_0, x2_0)
    x1_1 = x1_0 - A1/J
    x2_1 = x2_0 - A2/J
    if (abs(x1_0 - x1_1) > 0.01) | (abs(x2_0 - x2_1) > 0.01):
    	print(": x1 ="+str(x1_1)+" x2= "+str(x2_1))
    	Newton(x1_1, x2_1)
    return

Newton(1,1)
plt.plot()






