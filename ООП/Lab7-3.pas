{$N+}
program figures;
uses Crt, Graph;

type
    CoinPtr = ^Coin;
    Coin = object

    private
        pX, pY: ^integer;
        pR: ^integer;
        pFi: ^double;

    public
        constructor Init(InitX, InitY, InitR: integer; InitFi: double);
        destructor Del;
        function Get_X: integer;
        function Get_Y: integer;
        function Get_R: integer;
        function Get_Fi: double;
        procedure Put_X(NewX: integer);
        procedure Put_Y(NewY: integer);
        procedure Put_R(NewR: integer);
        procedure Put_Fi(NewFi: double);
        procedure Show;
        procedure Hide;
        procedure Slide(DX: integer);
        procedure Turn(DFi: double);
        procedure Roll(DFi: double);
end;

constructor Coin.Init(InitX, InitY, InitR: integer; InitFi: double);
    begin
        new(pX);
        new(pY);
        new(pR);
        new(pFi);
        pX^:=InitX;
        pY^:=InitY;
        pR^:=InitR;
        pFi^:=InitFi
    end;

destructor Coin.Del;
    begin
        dispose(pX);
        dispose(pY);
        dispose(pR);
        dispose(pFi);
    end;

function Coin.Get_X; begin Get_X:=pX^ end;

function Coin.Get_Y; begin Get_Y:=pY^ end;

function Coin.Get_R; begin Get_R:=pR^ end;

function Coin.Get_Fi; begin Get_Fi:=pFi^ end;

procedure Coin.Put_X(NewX: integer); begin pX^:=NewX end;

procedure Coin.Put_Y(NewY: integer); begin pY^:=NewY end;

procedure Coin.Put_R(NewR: integer); begin pR^:=NewR end;

procedure Coin.Put_Fi(NewFi: double); begin pFi^:=NewFi end;

procedure Coin. Show;
    var
        Rx, Ry : word;
        StAngle, EndAngle : word;
    begin
        StAngle := 0;
        EndAngle := 360;
        Ry := pR^;
        Rx := round(Abs((pR^)*cos(pFi^)));
        Ellipse(pX^, pY^, StAngle, EndAngle, Rx, Ry)
    end;

procedure Coin. Hide;
    var
        TempColor: word;
    begin
        TempColor:=GetColor;
        SetColor(GetBkColor);
        Show;
        SetColor(TempColor);
    end;

procedure Coin. Slide(DX: integer);
    begin
        Hide;
        Put_X(pX^+DX);
        Show
    end;

procedure Coin. Turn(DFi: double);
    begin
        Hide;
        Put_Fi(pFi^ +DFi);
        Show
    end;

procedure Coin. Roll(DFi: double);
    var
        DX:integer;
    begin
        Turn(DFi);
        DX:=round(DFi*pR^);
        Slide(DX)
    end;

    var
        gdriver, gmode, errcode: integer;
        pC1, pC2, pC3: CoinPtr;
    begin
        clrscr;
        gdriver:=detect;
        gmode:=detect;
        initgraph(gdriver, gmode,'');
        errcode:=GraphResult;
        if not (errcode = grOk) then
        begin
        writeln('Graphics error ', grapherrormsg(errcode));
        halt(1)
    end;
    setcolor(15);
   new(pC1);
    new(pC2);
    new(pC3);
    pC1^.Init(300, 100, 50, 0);
    pC2^.Init(100, 250, 50, 0);
    pC3^.Init(100, 400, 50, 0);
    pC1^.Show;
    pC2^.Show;
    pC3^.Show;
    readln;
    repeat
        pC1^.Turn(0.1);
        pC2^.Slide(5);
        if pC2^.Get_X>=700 then pC2^.Put_X(-50);
        pC3^.Roll(0.1);
        if pC3^.Get_X>=700 then pC3^.Put_X(-50); 
        delay(10); 
    until KeyPressed;
    pC1^.Hide;
    pC2^.Hide;
    pC3^.Hide;
    
closegraph;

    writeln('Before dispose dynamical object C1: X =', pC1^.Get_X, ', Y = ', 
        pC1^.Get_Y, ', R = ',pC1^.Get_R, ', Fi = ', pC1^.Get_Fi);
    readln;
    pC1^.Del;
    writeln('After destruct dynamical object C1: X =', pC1^.Get_X, ', Y = ', 
        pC1^.Get_Y, ', R = ',pC1^.Get_R, ', Fi = ', pC1^.Get_Fi);
    readln;
    dispose(pC1);
    writeln('After dispose dynamical object C1: X =', pC1^.Get_X, ', Y = ', 
        pC1^.Get_Y, ', R = ',pC1^.Get_R, ', Fi = ', pC1^.Get_Fi);
    readln;


    writeln('Before dispose dynamical object C2: X =', pC2^.Get_X, ', Y = ', 
        pC2^.Get_Y, ', R = ',pC2^.Get_R, ', Fi = ', pC2^.Get_Fi);
    readln;
    pC2^.Del;
    writeln('After destruct dynamical object C2: X =', pC2^.Get_X, ', Y = ', 
        pC2^.Get_Y, ', R = ',pC2^.Get_R, ', Fi = ', pC2^.Get_Fi);
    readln;
    dispose(pC2);
    writeln('After dispose dynamical object C2: X =', pC2^.Get_X, ', Y = ', 
        pC2^.Get_Y, ', R = ',pC2^.Get_R, ', Fi = ', pC2^.Get_Fi);
    readln;

    writeln('Before dispose dynamical object C3: X =', pC3^.Get_X, ', Y = ', 
        pC3^.Get_Y, ', R = ',pC3^.Get_R, ', Fi = ', pC3^.Get_Fi);
    readln;
    pC3^.Del;
    writeln('After destruct dynamical object C3: X =', pC3^.Get_X, ', Y = ', 
        pC3^.Get_Y, ', R = ',pC3^.Get_R, ', Fi = ', pC3^.Get_Fi);
    readln;
    dispose(pC3);
    writeln('After dispose dynamical object C3: X =', pC3^.Get_X, ', Y = ', 
        pC3^.Get_Y, ', R = ',pC3^.Get_R, ', Fi = ', pC3^.Get_Fi);
    readln;
end.

