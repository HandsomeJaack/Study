{$N+}
program figures;
uses Crt, Graph;
 
type
    PointPtr = ^Point;
    Point = object
 
    public
        X, Y, Color: integer;
    public
        constructor Init(InitX, InitY, InitColor: integer);
        function Get_X: integer;
        function Get_Y: integer;
        function Get_Color: integer;
        procedure PutX(NewX: integer);
        procedure PutY(NewY: integer);
        procedure PutColor(NewColor: integer);
        procedure MoveTo(DX, DY: integer);
        procedure Show; virtual;
        procedure Hide;
end;
 
type
    LinelPtr = ^Linel;
    Linel = object(Point)
 
    public
        Ang, len: double;
    public
        constructor Init(InitX, InitY, InitColor: integer; InitAng, InitLen: double);
        procedure PutAng(NewAng: double);
        procedure PutLEn(NewLen: double);
        function GetAng: double;
        function GetLen: double;
        procedure Show; virtual;
        procedure Rotate(DAng: double);
        procedure Expand(DAng: double);
end;
 
type
    SquarePtr = ^Square;
    Square = object(Linel)
 
    public
        constructor Init(InitX, InitY, InitColor: integer; InitAng, InitLen: double);
        procedure Show; virtual;
        function GetSquare: integer;
end;
 
type
    PyramidePtr = ^Pyramide;
    Pyramide = object(Square)
 
    public
        H: double;
    public
        constructor Init(InitX, InitY, InitColor: integer; InitAng, InitLen, InitH: double);
        procedure PutH(NewH: double);
        function GetH: double;
        procedure Show; virtual;
        procedure Expand(DAng: double);
        function GetVolume: integer;
end;
 
constructor Point.Init(InitX, InitY, InitColor: integer);
    begin
        X:=InitX;
        Y:=InitY;
        Color:=InitColor;
    end;
 
constructor Linel.Init(InitX, InitY, InitColor: integer; InitAng, InitLen: double);
    begin
        Point.Init(InitX, InitY, InitColor);
        Ang:=InitAng;
        len:=InitLen;
    end;
 
constructor Square.Init(InitX, InitY, InitColor: integer; InitAng, InitLen: double);
    begin
        Linel.Init(InitX, InitY, InitColor, InitAng, InitLen);
    end;
 
constructor Pyramide.Init(InitX, InitY, InitColor: integer; InitAng, InitLen, InitH: double);
    begin
        Square.Init(InitX, InitY, InitColor, InitAng, InitLen);
        H:=InitH
    end;
 
function Point.Get_X; begin Get_X:=X end;
function Point.Get_Y; begin Get_Y:=Y end;
function Point.Get_Color; begin Get_Color:=Color end;
procedure Point.PutX(NewX: integer); begin X:=NewX end;
procedure Point.PutY(NewY: integer); begin Y:=NewY end;
procedure Point.PutColor(NewColor: integer); begin Color:=NewColor end;
 
procedure Point. Show;
    begin
        putpixel(X,Y,Color);
    end;
 
procedure Point.Hide;
    var
        TempColor: integer;
    begin
        TempColor:=Color;
        PutColor(GetBkColor);
        Show;
        PutColor(TempColor);
    end;
    
procedure Point.MoveTo(DX,DY: integer);
    begin
        Hide;
        PutX(X+DX);
        PutY(Y+DY);
        Show;
    end;

procedure Linel.PutAng(NewAng: double); begin Ang:=NewAng end;
procedure Linel.PutLEn(NewLen: double); begin Len:=NewLen end;
function Linel.GetAng; begin GetAng:=Ang end;
function Linel.GetLen; begin GetLen:=len end;
 
procedure Linel.Show;
    var
        TempColor: integer;
        X1,Y1, X2,Y2: integer;
    begin
        TempColor:=GetColor;
        SetColor(Color);
        X1:= X + round(len*(Cos(Ang))/2.0);
        Y1:= Y + round(len*(Sin(Ang))/2.0);
        X2:= X - round(len*(Cos(Ang))/2.0);
        Y2:= Y - round(len*(Sin(Ang))/2.0);
        line(X1, Y1, X2, Y2);
        SetColor(TempColor)
    end;
 
procedure Linel.Rotate(DAng: double);
    begin
        Hide;
        PutAng(Ang +DAng);
        Show;
    end;
 
procedure Linel.Expand(DAng: double);
    begin
        Hide;
        len:= len + 5*cos(DAng);
        Show;
    end;
 
function Square.GetSquare; begin GetSquare:=round(len*len) end;
procedure Pyramide.PutH(NewH: double); begin H:=NewH; end;
function Pyramide.GetH; begin GetH:=H end;
 
procedure Square.Show;
    var
        TempColor: integer;
        X1,Y1,X2,Y2,X3,Y3,X4,Y4: double;
    begin
        TempColor:=GetColor;
        SetColor(Color);
        X1 := X + len*cos(Ang)/2;
        Y1 := Y + len/2 +sin(Ang)*len/4;
        X2 := X + cos(Ang)*len/2;
        Y2 := Y - len/2 + sin(Ang)*len/4;
        X3 := X - len*(cos(Ang))/2;
        Y3 := Y - len/2 -sin(Ang)*len/4;
        X4 := X - len*(cos(Ang))/2;
        Y4 := Y + len/2 - sin(Ang)*len/4;
        Line(round(X1),round(Y1),round(X2),round(Y2));
        Line(round(X2),round(Y2),round(X3),round(Y3));
        Line(round(X3),round(Y3),round(X4),round(Y4));
        Line(round(X4),round(Y4),round(X1),round(Y1));
        SetColor(TempColor)
    end;
 
function Pyramide.GetVolume; begin GetVolume:=round(len*len*H/3.0) end;

procedure Pyramide.Show;
    var
        TempColor: integer;
        X1,Y1,X2,Y2,X3,Y3,X4,Y4,X5,Y5: integer;
    begin
        TempColor:=GetColor;
        SetColor(Color);
        X1:= X + round(cos(Ang)*len - sin(Ang)*len);
        Y1:= Y + round(sin(Ang)*len + cos(Ang)*len);
        X2:= X + round(cos(Ang)*len + sin(Ang)*len);
        Y2:= Y + round(sin(Ang)*len - cos(Ang)*len);
        X3:= X - round(cos(Ang)*len + sin(Ang)*len);
        Y3:= Y - round(sin(Ang)*len - cos(Ang)*len);
        X4:= X - round(cos(Ang)*len - sin(Ang)*len);
        Y4:= Y - round(sin(Ang)*len + cos(Ang)*len);
        X5:= X;
        Y5:= Y - round(H);
        Line(X1,Y1,X2,Y2);
        Line(X2,Y2,X4,Y4);
        Line(X3,Y3,X1,Y1);
        Line(X4,Y4,X3,Y3);
        Line(X1,Y1,X5,Y5);
        Line(X2,Y2,X5,Y5);
        Line(X3,Y3,X5,Y5);
        Line(X4,Y4,X5,Y5);
        SetColor(TempColor)
    end;
 
procedure Pyramide.Expand(DAng: double);
    begin
        Hide;
        len:= len + 4*cos(DAng);
        H:= H + 6*cos(DAng);
        Show
    end;
 
    var
        gdriver, gmode, errcode: integer;
        P1,P2: Point;
        L1, L2: Linel;
        S1, S2: Square;
        R1, R2: Pyramide;
        Angle: double;
    begin
        clrscr;
        gdriver:=detect;
        gmode:=detect;
        initgraph(gdriver, gmode,'');
        errcode:=GraphResult;
        if not (errcode = grOk) then
        begin
        writeln('Graphics error ', grapherrormsg(errcode));
        halt(1)
    end;
    setcolor(15);
    P1.Init(25,25,15); 
    L1.Init(50,100,15,0,100); 
    S1.Init(250,300,27,0,100); 
    R1.Init(360,360,49,0,100,150);
    P1.Show; readln;
    L1.Show; readln;
    S1.Show; readln;
    R1.Show; readln;
    Angle := 135;    
    L1.MoveTo(45,0);
    S1.MoveTo(45,0);
    R1.MoveTo(45,0);
    readln;
    L1.Rotate(35);
    S1.Rotate(35);
    R1.Rotate(35);
    readln;
    L1.Expand(Angle);
    S1.Expand(Angle);
    R1.Expand(Angle);     
    readln;
closegraph;
end.
