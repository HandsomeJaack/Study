{$N+}
program figures;
uses Crt, Graph;

type
    CoinPtr = ^Coin;
    Coin = object

    private
        X, Y: integer;
        R: integer;
        Fi: double;

    public
        constructor Init(InitX, InitY, InitR: integer; InitFi: double);
        function Get_X: integer;
        function Get_Y: integer;
        function Get_R: integer;
        function Get_Fi: double;
        procedure Put_X(NewX: integer);
        procedure Put_Y(NewY: integer);
        procedure Put_R(NewR: integer);
        procedure Put_Fi(NewFi: double);
        procedure Show;
        procedure Hide;
        procedure Slide(DX: integer);
        procedure Turn(DFi: double);
        procedure Roll(DFi: double);
end;

constructor Coin.Init(InitX, InitY, InitR: integer; InitFi: double);
    begin
        X:=InitX;
        Y:=InitY;
        R:=InitR;
        Fi:=InitFi
    end;

function Coin.Get_X; begin Get_X:=X end;

function Coin.Get_Y; begin Get_Y:=Y end;

function Coin.Get_R; begin Get_R:=R end;

function Coin.Get_Fi; begin Get_Fi:=Fi end;

procedure Coin.Put_X(NewX: integer); begin X:=NewX end;

procedure Coin.Put_Y(NewY: integer); begin Y:=NewY end;

procedure Coin.Put_R(NewR: integer); begin R:=NewR end;

procedure Coin.Put_Fi(NewFi: double); begin Fi:=NewFi end;

procedure Coin. Show;
    var
        Rx, Ry : word;
        StAngle, EndAngle : word;
    begin
        StAngle := 0;
        EndAngle := 360;
        Ry := R;
        Rx := round(Abs(R*cos(Fi)));
        Ellipse(X, Y, StAngle, EndAngle, Rx, Ry)
    end;

procedure Coin. Hide;
    var
        TempColor: word;
    begin
        TempColor:=GetColor;
        SetColor(GetBkColor);
        Show;
        SetColor(TempColor);
    end;

procedure Coin. Slide(DX: integer);
    begin
        Hide;
        Put_X(X+DX);
        Show
    end;

procedure Coin. Turn(DFi: double);
    begin
        Hide;
        Put_Fi(Fi +DFi);
        Show
    end;

procedure Coin. Roll(DFi: double);
    var
        DX:integer;
    begin
        Turn(DFi);
        DX:=round(DFi*R);
        Slide(DX)
    end;

    var
        gdriver, gmode, errcode: integer;
        pC1, pC2, pC3: CoinPtr;
    begin
        clrscr;
        gdriver:=detect;
        gmode:=detect;
        initgraph(gdriver, gmode,'');
        errcode:=GraphResult;
        if not (errcode = grOk) then
        begin
        writeln('Graphics error ', grapherrormsg(errcode));
        halt(1)
    end;
    setcolor(15);
    new(pC1);
    new(pC2);
    new(pC3);
    pC1^.Init(300, 100, 50, 0);
    pC2^.Init(100, 250, 50, 0);
    pC3^.Init(100, 400, 50, 0);
    pC1^.Show;
    pC2^.Show;
    pC3^.Show;
    readln;
    repeat
        pC1^.Turn(0.1);
        pC2^.Slide(5);
        if pC2^.Get_X>=700 then pC2^.Put_X(-50);
        pC3^.Roll(0.1);
        if pC3^.Get_X>=700 then pC3^.Put_X(-50); 
        delay(10); 
    until KeyPressed;
    pC1^.Hide;
    pC2^.Hide;
    pC3^.Hide;
    
closegraph;

    writeln('Before dispose dynamical object C1: X =', pC1^.Get_X, ', Y = ', 
        pC1^.Get_Y, ', R = ',pC1^.Get_R, ', Fi = ', pC1^.Get_Fi);
    readln;
    dispose(pC1);
    writeln('After dispose dynamical object C1: X =', pC1^.Get_X, ', Y = ', 
        pC1^.Get_Y, ', R = ',pC1^.Get_R, ', Fi = ', pC1^.Get_Fi);
    readln;

    writeln('Before dispose dynamical object C2: X =', pC2^.Get_X, ', Y = ', 
        pC2^.Get_Y, ', R = ',pC2^.Get_R, ', Fi = ', pC2^.Get_Fi);
    readln;
    dispose(pC2);
    writeln('After dispose dynamical object C2: X =', pC2^.Get_X, ', Y = ', 
        pC2^.Get_Y, ', R = ',pC2^.Get_R, ', Fi = ', pC2^.Get_Fi);
    readln;
    
    writeln('Before dispose dynamical object C3: X =', pC3^.Get_X, ', Y = ', 
        pC3^.Get_Y, ', R = ',pC3^.Get_R, ', Fi = ', pC3^.Get_Fi);
    readln;
    dispose(pC3);
    writeln('After dispose dynamical object C3: X =', pC3^.Get_X, ', Y = ', 
        pC3^.Get_Y, ', R = ',pC3^.Get_R, ', Fi = ', pC3^.Get_Fi);
    readln;
end.

