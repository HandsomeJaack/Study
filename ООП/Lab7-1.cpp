#include <graphics.h>
#include <iostream.h>
#include <conio.h>
#include <math.h>
#include <dos.h>

class Coin
{
    private:
        int X, Y, R;
        double Fi;
    public:
        Coin(int, int, int, double);
        void PutX(int);
        void PutY(int);
        void PutR(int);
        void PutFi(double);
        int GetX();
        int GetY();
        int GetR();
        double GetFi();
        void Show();
        void Hide();
        void Slide(int);
        void Turn(double);
        void Roll(double);
};

Coin::Coin(int X, int Y, int R, double Fi)
{
    this ->X=X;
    this ->Y=Y;
    this -> R=R;
    this ->Fi=Fi;
}

void Coin ::PutX(int X) { this -> X=X; }

void Coin ::PutY(int Y) { this -> Y=Y; }

void Coin ::PutR(int R) { this -> R=R; }

void Coin ::PutFi(double Fi) { this -> Fi=Fi; }

int Coin ::GetX() { return (X); }

int Coin ::GetY() { return (Y); }

int Coin ::GetR() { return (R); }

double Coin ::GetFi() { return (Fi); }

void Coin ::Show()
{
    int Ry = R;
    int Rx = abs(R*cos(Fi));
    ellipse(X,Y,0,360,Rx,Ry);
}

void Coin::Hide()
{
    unsigned TempColor;
    TempColor=getcolor();
    setcolor(getbkcolor());
    Show();
    setcolor(TempColor);
}

void Coin::Slide(int DX)
{
    Hide();
    PutX(X+DX);
    Show();
}

void Coin::Turn(double DFi)
{
    Hide();
    PutFi(Fi+DFi);
    Show();
}
void Coin::Roll(double DFi)
{
    Turn(DFi);
    int DX=R*DFi;
    Slide(DX);
}

int main()
{
    int gdriver = DETECT, gmode, errorcode;
    initgraph(&gdriver, &gmode, "");
    errorcode = graphresult();
    if (errorcode != grOk)
    {
        cout<<"Graphics error: "<<grapherrormsg(errorcode)<<endl;
        cout<<"Press any key to halt:"<<endl;
        getch();
        return(1);
    }
    setcolor(15);

    Coin *pC1, *pC2, *pC3;
    pC1 = new Coin(320,100,50,0);
    pC2 = new Coin(100,250,50,0);
    pC3 = new Coin(100,400,50,0);
    pC1 -> Show();
    pC2 -> Show();
    pC3 -> Show();
    getch();
    while(!kbhit())
    {
        pC1 -> Turn(0.1);
        pC2 -> Slide(50*0.1);
        if(pC2 -> GetX()>=700) pC2 -> PutX(-50);
        pC3 -> Roll(0.1);
        if(pC3 ->GetX()>=700) pC3 ->PutX(-50);
        delay(100);
    }
    pC1 -> Hide();
    pC2 -> Hide();
    pC3 -> Hide();
    closegraph();
    getch();

    cout<<"Before deleting dynamical object C1: X = "<<pC1 ->GetX()<<
    ", Y = "<<pC1 ->GetY()<<", R = "<< pC1 ->GetR()<<", Fi = "<<pC1 ->GetFi()<<endl;
    getch();
    delete pC1;
    cout<<"After deleting dynamical object C1: X = "<<pC1 ->GetX()<<
    ", Y = "<<pC1 ->GetY()<<", R = "<< pC1 ->GetR()<<", Fi = "<<pC1 ->GetFi()<<endl;
    getch();

    cout<<"\nBefore deleting dynamical object C2: X = "<<pC2 ->GetX()<<
    ", Y = "<<pC2 ->GetY()<<", R = "<< pC2 ->GetR()<<", Fi = "<<pC2 ->GetFi()<<endl;
    getch();
    delete pC2;
    cout<<"After deleting dynamical object C2: X = "<<pC2 ->GetX()<<
    ", Y = "<<pC2 ->GetY()<<", R = "<< pC2 ->GetR()<<", Fi = "<<pC2 ->GetFi()<<endl;
    getch();

    cout<<"\nBefore deleting dynamical object C3: X = "<<pC3 ->GetX()<<
    ", Y = "<<pC3 ->GetY()<<", R = "<< pC3 ->GetR()<<", Fi = "<<pC3 ->GetFi()<<endl;
    getch();
    delete pC3;
    cout<<"After deleting dynamical object C3: X = "<<pC3 ->GetX()<<
    ", Y = "<<pC3 ->GetY()<<", R = "<< pC3 ->GetR()<<", Fi = "<<pC3 ->GetFi()<<endl;
    getch();

    return(0);
};
