{$N+}
program figures;
uses Crt, Graph, Objects;

type
    PointPtr = ^Point;
    Point = object
 
    public
        X,Y,Color: integer;
    public
        constructor Init(InitX, InitY, InitColor: integer);
        function Get_X: integer;
        function Get_Y: integer;
        procedure PutX(NewX: integer);
        procedure PutY(NewY: integer);
        function Get_Color: integer;
        procedure PutColor(NewColor: integer);
        procedure Show;
end;
 
type
    LinelPtr = ^Linel;
    Linel = object
    public
        P1: Point;
        P2: Point;
        constructor Init(InitP1, InitP2: Point);
        procedure Show;
end;
 
type
    SquarePtr = ^Square;
    Square = object(Linel)
    public
        P3: Point;
        P4: Point;
        constructor Init(InitP1, InitP2, InitP3, InitP4: Point);
        procedure Show;
end;
 
type
    PyramidePtr = ^Pyramide;
    Pyramide = object(Square)
    public
        P5: Point;
        constructor Init(InitP1, InitP2, InitP3, InitP4, InitP5: Point);
        procedure Show;
end;

constructor Point.Init(InitX, InitY, InitColor: integer);
    begin
        X:=InitX;
        Y:=InitY;
        Color:=InitColor;
    end;
 
constructor Linel.Init(InitP1, InitP2: Point);
    begin
        P1.X:=InitP1.Get_X;
        P1.Y:=InitP1.Get_Y;
        P2.X:=InitP2.Get_X;
        P2.Y:=InitP2.Get_Y
    end;
 
constructor Square.Init(InitP1, InitP2, InitP3, InitP4: Point);
    begin
        P1.X:=InitP1.Get_X;
        P1.Y:=InitP1.Get_Y;
        P2.X:=InitP2.Get_X;
        P2.Y:=InitP2.Get_Y;
        P3.X:= InitP3.Get_X;
        P3.Y:= InitP3.Get_Y;
        P4.X:= InitP4.Get_X;
        P4.Y:= InitP4.Get_Y
    end;
 
constructor Pyramide.Init(InitP1, InitP2, InitP3, InitP4, InitP5: Point);
    begin
        P1.X:=InitP1.Get_X;
        P1.Y:=InitP1.Get_Y;
        P2.X:=InitP2.Get_X;
        P2.Y:=InitP2.Get_Y;
        P3.X:= InitP3.Get_X;
        P3.Y:= InitP3.Get_Y;
        P4.X:= InitP4.Get_X;
        P4.Y:= InitP4.Get_Y;
        P5.X:= InitP5.Get_X;
        P5.Y:= InitP5.Get_Y
    end;
 
function Point.Get_X; begin Get_X:=X end;
function Point.Get_Y; begin Get_Y:=Y end;
function Point.Get_Color; begin Get_Color:=Color end;
procedure Point.PutX(NewX: integer); begin X:=NewX end;
procedure Point.PutY(NewY: integer); begin Y:=NewY end;
procedure Point.PutColor(NewColor: integer); begin Color:=NewColor end;

procedure Point.Show; begin PutPixel(X, Y, Color) end;
 
procedure Linel.Show;
    begin
        Line(P1.Get_X, P1.Get_Y, P2.Get_X, P2.Get_Y)
    end;
 
procedure Square.Show;
    begin
        Line(P1.Get_X, P1.Get_Y, P2.Get_X, P2.Get_Y);
        Line(P2.Get_X, P2.Get_Y, P3.Get_X, P3.Get_Y);
        Line(P3.Get_X, P3.Get_Y, P4.Get_X, P4.Get_Y);
        Line(P4.Get_X, P4.Get_Y, P1.Get_X, P1.Get_Y)
    end;

procedure Pyramide.Show;
    begin
        Line(P1.Get_X, P1.Get_Y, P2.Get_X, P2.Get_Y);
        Line(P2.Get_X, P2.Get_Y, P3.Get_X, P3.Get_Y);
        Line(P3.Get_X, P3.Get_Y, P4.Get_X, P4.Get_Y);
        Line(P4.Get_X, P4.Get_Y, P1.Get_X, P1.Get_Y);
        Line(P1.Get_X, P1.Get_Y, P5.Get_X, P5.Get_Y);
        Line(P2.Get_X, P2.Get_Y, P5.Get_X, P5.Get_Y);
        Line(P3.Get_X, P3.Get_Y, P5.Get_X, P5.Get_Y);
        Line(P4.Get_X, P4.Get_Y, P5.Get_X, P5.Get_Y)
    end;
 
    var
        gdriver, gmode, errcode: integer;
        P1,P2,P3, P4, P5: Point;
        L: Linel;
        S: Square;
        R: Pyramide;
    begin
        clrscr;
        gdriver:=detect;
        gmode:=detect;
        initgraph(gdriver, gmode,'');
        errcode:=GraphResult;
        if not (errcode = grOk) then
        begin
        writeln('Graphics error ', grapherrormsg(errcode));
        halt(1)
    end;
    P1.Init(100,250, 8);
    P2.Init(200,250, 8);
    P3.Init(200,150,8);
    P4.Init(100,150,8);
    P5.Init(150,100,8);
    P1.Show; P2.Show; readln;
    L.Init(P1,P2);
    L.Show; readln;
    S.Init(P1,P2,P3,P4); 
    S.Show; readln;
    R.Init(P1,P2,P3,P4,P5);
    R.Show; readln;
    readln;
    closegraph;
end.
